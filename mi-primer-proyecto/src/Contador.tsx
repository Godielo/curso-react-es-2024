import { useState } from "react"

export const Contador = () => {

    const initialState: number = 0

    const [contador, setContador] = useState(initialState)

    const incrementar = () => setContador(contador+1)

    const decrementar = () => setContador(contador-1)
    
    const resetear = () => setContador(initialState)

    return (
        <>
            <h1>Contador: {contador}</h1>
            <hr />
            <button onClick={incrementar}>Incrementar</button>
            <button onClick={decrementar}>Decrementar</button>
            <button onClick={resetear}>Resetear</button>
        </>
    )
}
