export interface Categorias {
    id: number,
    tipo: string,
    descripcion: string
}