import { useState } from "react"

export const useForm = <T extends object>(estadoInicialFormulario: T) => {
    const [formulario, setFormulario] = useState(estadoInicialFormulario)
    const onChange = <K extends NonNullable<unknown>>(campo: keyof T, valor: K) => {
        setFormulario(
            {
                ...formulario, [campo]: valor
            }
        )
    }
    const resetFormulario = () => {
        setFormulario(estadoInicialFormulario)
    }
    return {
        ...formulario, formulario, onChange, resetFormulario
    }
}
