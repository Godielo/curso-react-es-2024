import { Navigate, Route, Routes } from "react-router-dom"
import { InicioPage } from "../pages/InicioPage"
import { ProductosPage } from "../pages/ProductosPage"
import { UsuariosPage } from "../pages/UsuariosPage"
import { CategoriasPage } from "../pages/CategoriasPage"

export const AppRouter = () => {
    return (
        <Routes>
            <Route path="/*" element={<Navigate to={'/inicio'}/>} />
            <Route path="inicio" element={<InicioPage/>} />
            <Route path="productos" element={<ProductosPage/>} />
            <Route path="usuarios" element={<UsuariosPage/>} />
            <Route path="categorias" element={<CategoriasPage/>} />
        </Routes>
    )
}
