import { useNavigate } from 'react-router-dom'

export const BotonVolver = () => {
    const navigate = useNavigate()
    return (
        <div style={{marginTop: 30}}>
            <button className='btn btn-danger' onClick={()=>navigate(-1)}>Volver</button>
        </div>
    )
}
