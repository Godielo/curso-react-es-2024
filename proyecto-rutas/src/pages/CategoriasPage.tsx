// import { useNavigate } from "react-router-dom"
import { BotonVolver } from "../components/BotonVolver"
import { useState } from "react"
import { Categorias } from "../interfaces/Categoria.interface"
import { useForm } from "../hooks/useForm"
import { BotonNavegador } from "../components/BotonNavegador"

const categoriaInicialForm = {
    tipo: '', descripcion: ''
}

export const CategoriasPage = () => {
    const [categorias, setCategorias] = useState<Categorias[]>([])
    const {tipo, descripcion, onChange, resetFormulario} = useForm(categoriaInicialForm)
    const agregarCategoria = (tipo: string, descripcion: string) => {
        const nuevaCategoria = {
            id: categorias.length + 1,
            tipo,
            descripcion
        }
        setCategorias(categorias.concat(nuevaCategoria))
    }
    const onAgregarClicked = () => {
        if (tipo.length === 0 || descripcion.length === 0) return;
        agregarCategoria(tipo, descripcion)
        resetFormulario()
    }
    // const navigate = useNavigate()
    // const navegarAProductos = () => navigate('/productos')
    return (
        <>
        <h1>Categorias</h1>
        <hr />
        <table className="table table-bordered">
            <thead>
                <tr>
                    <th className="text-center">ID</th>
                    <th className="text-center">Tipo</th>
                    <th className="text-center">Descripción</th>
                </tr>
            </thead>
            <tbody>
            {
                categorias.map(categoria => (
                    <tr key={categoria.id}>
                        <td className="text-center">{categoria.id}</td>
                        <td>{categoria.tipo}</td>
                        <td>{categoria.descripcion}</td>
                    </tr>
                ))
            }
            </tbody>
        </table>
        <br />
        <form onSubmit={(event) => {
            event.preventDefault()
            onAgregarClicked()}}>
            <div className="input-group">
                <input className="form-control" type="text" name="tipo" id="tipo" required placeholder="Tipo" style={{marginRight: 2}} value={tipo} onChange={(event) => {
                    onChange('tipo', event.target.value)
                }} />
                <input className="form-control" type="text" name="descripcion" id="descripcion" required placeholder="Descripción" style={{marginRight: 2}} value={descripcion} onChange={(event) => {
                    onChange('descripcion', event.target.value)
                }} />
                <button className="btn btn-success" type="submit" style={{marginRight: 2}} >
                    Agregar Categoria
                </button>
            </div>
        </form>
        <br />
        {/* <button className="btn btn-primary" onClick={navegarAProductos} style={{marginLeft: 2}}>Productos</button> */}
        <BotonNavegador name="Productos" ruta="/productos" />
        <BotonVolver/>
        </>
    )
}
