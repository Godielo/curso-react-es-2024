import { useState } from "react"
import { BotonVolver } from "../components/BotonVolver"
import { Producto } from '../interfaces/Producto.interface';
import { useForm } from "../hooks/useForm";

const estadoInicialFormulario = {
    descripcion: '', precio: ''
}

export const ProductosPage = () => {
    const [productos, setProductos] = useState<Producto[]>([])
    const {descripcion, precio, onChange, resetFormulario} = useForm(estadoInicialFormulario)
    const agregarProducto = (descripcion: string, precio: string) => {
        const nuevoProducto = {
            id: productos.length + 1,
            descripcion,
            precio
        }
        setProductos(productos.concat(nuevoProducto))
    }
    const onAgregarClicked = () => {
        if (descripcion.length === 0 || precio.length === 0) return;
        agregarProducto(descripcion, precio)
        resetFormulario()
    }
    return (
        <>
        <h1>Productos</h1>
        <hr />
        {/* <ul>
        {
            productos.map(producto => <li key={producto.id}>{producto.descripcion} - {producto.precio}</li>)
        }
        </ul> */}
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                </tr>
            </thead>
            <tbody>
            {
                productos.map(producto => (
                    <tr key={producto.id}>
                        <td>{producto.id}</td>
                        <td>{producto.descripcion}</td>
                        <td>{producto.precio}</td>
                    </tr>
                ))
            }
            </tbody>
            <tfoot></tfoot>
        </table>
        <form onSubmit={(event) => {
            event.preventDefault()
            onAgregarClicked()}}>
            <div className="input-group">
                <input className="form-control" type="text" name="descripcion" id="descripcion" required placeholder="Descripcion" style={{marginRight: 2}} value={descripcion} onChange={(event) => {
                    onChange('descripcion', event.target.value)
                }} />
                <input className="form-control" type="number" name="precio" id="precio" required placeholder="Precio" value={precio} onChange={(event) => {
                    onChange('precio', event.target.value)
                }} />
                <button className="btn btn-success col-3" type="submit" style={{marginRight: 2}} >
                    Agregar Producto
                </button>
            </div>
        </form>
        <BotonVolver/>
        </>
    )
}