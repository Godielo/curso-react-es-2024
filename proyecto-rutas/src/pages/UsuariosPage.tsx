import { useState } from "react"
import { BotonVolver } from "../components/BotonVolver"
import { UsuariosInterface } from "../interfaces/Usuario.interface"
import { useForm } from "../hooks/useForm"

const listaInicialUsuarios = {
    nombre: '', apellido: ''
}

export const UsuariosPage = () => {
    
    const [usuarios, setUsuarios] = useState<UsuariosInterface[]>([])
    const {nombre, apellido, onChange, resetFormulario} = useForm(listaInicialUsuarios)
    const agregarUsuario = (nombre: string, apellido: string) => {
        const nuevoUsuario = {
            id: usuarios.length + 1,
            nombre,
            apellido
        }
        setUsuarios(usuarios.concat(nuevoUsuario))
    }

    const onAgregarClicked = () => {
        if (nombre.length === 0 || apellido.length === 0) return;
        agregarUsuario(nombre, apellido)
        resetFormulario()
        // setNombre('')
        // setApellido('')
    }

    return (
        <>
        <h1>Usuarios</h1>
        <hr />
        <ul>
            {
                usuarios.map(usuario => <li key={usuario.id}>{usuario.nombre} - {usuario.apellido}</li>)
            }
        </ul>
        <form onSubmit={(event) => {
            event.preventDefault()
            onAgregarClicked()
        }}>
            <div className="input-group">
                <input className="form-control" type="text" name="nombre" id="nombre" required placeholder="Nombre" style={{marginRight: 2}} value={nombre} onChange={(event) => {
                    onChange('nombre', event.target.value)
                }} />
                <input className="form-control" type="text" name="apellido" id="apellido" placeholder="Apellido" style={{marginRight: 2}} value={apellido} onChange={(event) => {
                    onChange('apellido', event.target.value)
                }} />
                <button className="btn btn-success col-3" type="submit" style={{marginRight: 2}}>
                    Agregar Usuario
                </button>
            </div>
        </form>
        <BotonVolver/>
        </>
    )
}