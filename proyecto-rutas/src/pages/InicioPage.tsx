import { BotonNavegador } from "../components/BotonNavegador"

export const InicioPage = () => {

    return (
        <>
        <h1>Inicio</h1>
        <hr />
        <BotonNavegador name="Pruductos" ruta="/productos"/>
        <BotonNavegador name="Usuarios" ruta="/usuarios"/>
        <BotonNavegador name="Categorias" ruta="/categorias"/>
        </>
    )
}
